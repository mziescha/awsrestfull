package de.zieschang.info.pg11;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PGHelper {
	public static void createTable(Connection connection, String name) {
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String createSql = "CREATE TABLE IF NOT EXISTS " + name + "( did_it integer, set_name varchar(40) )";

		try {
			stmt.executeUpdate(createSql);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//return PGHelper.getTable(connection, name);
	}
	
	public static void getTable(Connection connection, String name) {
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String createSql = "SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_NAME = '" + name + "'";

		try {
			ResultSet rs = stmt.executeQuery(createSql);
			while (rs.next())
			{
			    System.out.print("Column 1 returned ");
			    System.out.println(rs.getString(1));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
