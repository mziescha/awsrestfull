package de.zieschang.info.aws.s3;

import java.util.LinkedList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class AWSS3Infos {

	public AmazonS3InfoSummary getSummary(AmazonS3 s3) {
		long totalSize = 0;
		int totalItems = 0;
		int totalBuckets = 0;
		LinkedList<String> aBuckets = new LinkedList<String>();
		for (Bucket bucket : s3.listBuckets()) {
			totalBuckets++;
			aBuckets.add(bucket.getName());
			ObjectListing objects = null;
			try {
				objects = s3.listObjects(bucket.getName());
			} catch (AmazonServiceException ase) {
				System.out.println("Error Message:    " + ase.getMessage());
				System.out.println("HTTP Status Code: " + ase.getStatusCode());
				System.out.println("AWS Error Code:   " + ase.getErrorCode());
				System.out.println("Error Type:       " + ase.getErrorType());
				System.out.println("Request ID:       " + ase.getRequestId());
			} catch (AmazonClientException ace) {
				System.out.println("Error Message: " + ace.getMessage());
				continue;
			}
			List<S3ObjectSummary> objectSummaries = null;
			try {
				objectSummaries = objects.getObjectSummaries();
			} catch (NullPointerException e) {
				System.out.println("Error Message:    " + e.getMessage());
				System.out.println("Bucket:           " + bucket.getName() + " has no summaries!");
				continue;
			}

			for (S3ObjectSummary objectSummary : objectSummaries) {
				totalSize += objectSummary.getSize();
				totalItems++;
			}
		}
		
		AmazonS3InfoSummary summary = new AmazonS3InfoSummary();
		summary.setTotalBucketCount(totalBuckets);
		summary.setTotalItems(totalItems);
		summary.setTotalSize(totalSize);
		summary.setBuckets(aBuckets.toArray());
		return summary;
	}
}
