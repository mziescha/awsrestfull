package de.zieschang.info.aws.s3;

public class AmazonS3InfoSummary {

	private Object[] aBuckets;
	private int totalBucketCount;
	private long totalSize;
	private int totalItems;

	public Object[] getBuckets() {
		return aBuckets;
	}

	public void setBuckets(Object[] aBuckets) {
		this.aBuckets = aBuckets;
	}

	public int getTotalBucketCount() {
		return totalBucketCount;
	}

	public void setTotalBucketCount(int totalBucketCount) {
		this.totalBucketCount = totalBucketCount;
	}

	public long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(long totalSize2) {
		this.totalSize = totalSize2;
	}

	public int getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(int totalItems) {
		this.totalItems = totalItems;
	}

}
