package de.zieschang.info.aws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import de.zieschang.info.aws.s3.AWSS3Infos;
import de.zieschang.info.aws.s3.AmazonS3InfoSummary;

@Path("/v1")
public class AwsRestInfos {
	//
	// http://localhost:8080/contextPath/rest/aws/{service}/{info}
	// Example:
	// http://localhost:8080/contextPath/rest/aws/s3/infos
	//
	@Path("{service}/{info}")
	@GET
	@Produces("application/json")
	public String getS3Info_JSON(@PathParam("service") String service, @PathParam("info") String info) {
		StringBuilder strBuilder = new StringBuilder("{");
		if (service.equals("s3")) {
			AWSUtils aws = null;
			try {
				aws = new AWSUtils().init();
			} catch (Exception e) {
				e.printStackTrace();
			}
			AmazonS3InfoSummary summary = new AWSS3Infos().getSummary(aws.getS3());
			strBuilder.append(JSONHelper.append("buckets", summary.getBuckets(), false));
			strBuilder.append(JSONHelper.append("totalBuckets", summary.getTotalBucketCount(), false));
			strBuilder.append(JSONHelper.append("totalItems", summary.getTotalItems(), false));
			strBuilder.append(JSONHelper.append("totalSize", summary.getTotalSize(), true));
		}else if (service.equals("ec2")) {
			
		}else if (service.equals("rds")) {
			
		}
		strBuilder.append("}");
		return strBuilder.toString();
	}

}