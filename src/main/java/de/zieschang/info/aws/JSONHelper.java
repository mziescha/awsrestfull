package de.zieschang.info.aws;

public class JSONHelper {
	public static String append(String key, int value, boolean isLast) {
		return JSONHelper.append(key, value+"", isLast);
	}

	public static String append(String key, long value, boolean isLast) {
		return JSONHelper.append(key, value+"", isLast);
	}

	public static String appendLast(String key, String value, boolean isLast) {
		return JSONHelper.append(key, value+"", isLast);
	}

	public static String append(String key, String value, boolean isLast) {
		if (isLast) {
			return  "\"" +key+ "\": \"" + value + "\"";
		}else {
			return  "\"" +key+ "\": \"" + value + "\",";	
		}
	}

	public static String append(String key, Object[] array, boolean isLast) {
		
		StringBuilder tmp = new StringBuilder("\""+ key + "\":[");
		for (int i = 0; i < array.length; i++) {
			tmp.append("\"" + array[i] + "\"");
			if ((i+1) < array.length) tmp.append(",");
		}
        tmp.append("]");

        if (!isLast) tmp.append(",");	

        return tmp.toString();
	}
}
